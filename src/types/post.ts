export interface PostI {
    id: number;
    username: string;
    date: string;
    title: string;
    content: string;
    imageUrl: string;
}
  