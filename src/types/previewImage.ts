export interface PreviewImageI {
    title: string;
    imageUrl: string;
}
  