import React, { useEffect, useRef } from "react";

interface PreviewImageProps {
  isOpen: boolean;
  onClose: () => void;
  imageUrl: string;
  title: string;
}

const PreviewImage: React.FC<PreviewImageProps> = ({
  isOpen,
  onClose,
  imageUrl,
  title,
}) => {
    const firstRender = useRef(false);
    useEffect(() => {
        if (firstRender.current) return;
        firstRender.current = true;
        
        console.log("Modal isOpen status: ", isOpen);
      }, [isOpen]);
    
  if (!isOpen) return null;
  return (
    <>
      {isOpen && (
        <div className="fixed inset-0 bg-black bg-opacity-50 z-50 flex justify-center items-center">
          <div className="bg-white p-4 rounded-lg max-w-3xl w-full">
            <div className="clear-both text-lg mb-2 font-medium">{title}</div>
            <img
              src={imageUrl}
              alt={title}
              className="w-full object-contain h-auto"
            />
            <button
              className="font-bold text-lg w-full mt-2 bg-gray-400 rounded-md hover:bg-gray-300"
              onClick={onClose}
            >
              Close
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default PreviewImage;
