import React, { useState } from "react";
import { PostI } from "../types";

interface PostListProps {
  post: PostI;
  index: number;
  openModal: (title: string, imageUrl: string) => void;
}

const PostList: React.FC<PostListProps> = ({ post, index, openModal }) => {
  const bgColor = index % 2 === 0 ? "bg-white" : "bg-[#ccecff]";

  return (
    <div
      className={`py-2 ${bgColor} rounded-sm shadow my-4 cursor-pointer`}
      onClick={() => openModal(post.title, post.imageUrl)}
    >
      <div className="flex justify-between items-center border-b-gray-300 border-b">
        <div className="flex items-center text-xs font-semibold p-2 px-4">
          <img
            className="w-6 h-6 rounded-full"
            src="/images/avatar.jpg"
            alt="Rounded avatar"
          />
          <span className="text-[#f84c21] text-sm mx-2">{post.username}</span>{" "}
          <span className="text-gray-500">posted on {post.date}</span>
        </div>
      </div>
      <div className="flex rounded overflow-hidden p-2 px-4">
        <img
          src={post.imageUrl}
          alt={post.title}
          className="w-1/3 object-cover h-40"
        />
        <div className="w-2/3 px-2">
          <h2 className="font-bold text-md mb-2">{post.title}</h2>
          <p className="text-gray-500 text-sm">{post.content}</p>
        </div>
      </div>
    </div>
  );
};

export default PostList;
