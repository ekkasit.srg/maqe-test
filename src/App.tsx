import React, { useCallback, useState } from "react";
import { useRecoilValue } from "recoil";
import PostList from "./components/PostList";
import { postsState } from "./state/postsState";
import PreviewImage from "./components/PreviewImage";
import { PreviewImageI } from "./types";

const App: React.FC = () => {
  const posts = useRecoilValue(postsState);
  const [isModalOpen, setModalOpen] = useState<boolean>(false);
  const [previewImg, setPreviewImg] = useState<PreviewImageI>({
    title: "",
    imageUrl: "",
  });

  const openModal = (title: string, imageUrl: string) => {
    setPreviewImg({
      title,
      imageUrl,
    });
    setModalOpen(true);
  };

  const closeModal = () => {
    setModalOpen(false);
  };
  const getUserTimezone = useCallback(() => {
    return Intl.DateTimeFormat().resolvedOptions().timeZone;
  }, []);

  const timezone = getUserTimezone();
  return (
    <div className="bg-[#eeeeee] min-h-screen">
      <div className="max-w-4xl mx-auto py-10 ">
        <h1 className="text-3xl font-bold text-gray-800">MAQE Forum</h1>
        <p className="text-gray-500">Your current timezone is: {timezone}</p>
        {posts.map((post, index) => (
          <PostList
            openModal={openModal}
            key={index}
            post={post}
            index={index}
          />
        ))}
      </div>
      {isModalOpen && (
        <PreviewImage
          key={isModalOpen ? "open" : "closed"}
          isOpen={isModalOpen}
          onClose={closeModal}
          imageUrl={previewImg.imageUrl}
          title={previewImg.title}
        />
      )}
    </div>
  );
};

export default App;
