import { atom } from 'recoil';
import { PostI } from '../types';

export const postsState = atom<PostI[]>({
  key: 'postsState', 
  default: [
    {
      id: 1,
      username: "Jason Bourne",
      date: "May 9, 2020, 00:01",
      title: "Let's see this awesome post!",
      content: "I'm really glad to see this forum's popularity!",
      imageUrl: "/images/post-1.webp",
    },
    {
      id: 2,
      username: "Boromir",
      date: "April 15, 2020, 13:33",
      title: "One Does Not Simply Walk into Mordor",
      content: "After day, don't sixth were divide creepeth living him it heaven moveth of male man rule, made gathered cattle after. For gathering place seed bearing called and let shall fly fruitful fruitful creeping them brought years beginning air to beginning, good don't one abundantly give. That were blessed made moving light saw place they're. Moved his life moved open for midst sea called grass, beast very beast third third. May let open seasons creepeth and created heaven fly you're let winged light. Fruit after. You'll face saying image yielding unto also can't them seed that shall, of together void image.",
      imageUrl: "/images/post-2.jpeg",
    }
  ]
});
