# MAQE Forum

This project, **MAQE Forum**, is a web application developed with React and styled with TailwindCSS. It features a dynamic display of forum posts and allows users to view larger images of these posts in a modal.

## Features

- **Dynamic Post Listing**: Renders posts from state management dynamically.
- **Modal Image Preview**: Users can click on post images to view them in a larger modal.
- **Responsive Design**: Built with TailwindCSS to ensure responsiveness across various devices.
- **Display User's Timezone**: Automatically detects and displays the user's current timezone.

## Prerequisites

Before running this project, ensure you have the following installed:
- Node.js (version 14 or higher)
- npm or Yarn (package manager)

## Installation

Clone the repository and install the dependencies:

```bash
git clone https://gitlab.com/ekkasit.srg/maqe-test.git
cd maqe-forum
npm install


### Notes:

- **Username**: The username for any configurations or examples listed in this README is `ekkasit.srg`.
- **Email**: For any inquiries or contributions, you can reach out via email at [ekkasit.srg@gmail.com](mailto:ekkasit.srg@gmail.com).

Please replace these details with your own if you fork or clone this repository for personal or organizational use.
